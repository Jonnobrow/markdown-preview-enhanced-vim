# Markdown Preview Enhanced for VIM

## Benefits over other vim markdown previews
- Better theming 
- Math rendering

## Plan
- [ ] Make an executable out of MUME that takes the markdown file to preview
- [ ] Create the vim plugin that uses the MUME executable in order to create a live preview

## Credit
- This uses [mume](https://github.com/shd101wyy/mume) as a backend
